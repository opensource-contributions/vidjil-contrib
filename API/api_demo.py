#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This file is a demonstration on a way to create sets, upload samples in them and launch analysis on a server.
It is based on work done at La Pitié Salpetrière, Paris and use in their regular worklfow.

You need to get api_vidjil.py file corresponding for your server:
* Server based on web2py (<2024) at https://gitlab.inria.fr/vidjil/vidjil/-/blob/d40562de3ff572747f0bafe3ff940a1b2f61e215/tools/api_vidjil.py for the lastest stable version.
* Server based on py4web (since 2024) at https://gitlab.inria.fr/vidjil/vidjil/-/blob/dev/tools/api_vidjil.py for the lastest version.

It use a CSV file with some defined fields (first_name,last_name,birth,comments,file_r1,file_r2,sampling_date,info,generic_set)
Each line is a sample that should be associated with:
* a patient, create each time, even if already present
* a run, created if not present
* an optionnal generic set, also created if not already present
After upload of sample, an analysis is automatically launch with config IGH (take care that config id may differ on each server).

## User and password can be hardcoded in file to be use for login (see variables LOCAL_USER and LOCAL_PASSWORD).
## If not define here, a prompt will appear at launch to fill these field
## The best security mix is to store your email login and to fill password with the integrated prompt at launch.

## !!! BE AWARE TO NOT SHARE A SCRIPT WITH PASSWORD FOR SECURITY REASONS !!!
"""


from api_vidjil import Vidjil
import argparse
import os
import getpass


### Target server (see doc/server.md)
## See documentation to find how to get ssl certificate
LOCAL_SERVER = "https://localhost/vidjil/"
LOCAL_SSL = "localhost-chain.pem"
# LOCAL_USER = "your user mail"
# LOCAL_PASSWORD = "your password"


## Parser, allow to get parameters to execute this scripts
parser = argparse.ArgumentParser(description= 'Vidjil API workflow of La Pitié Salpetrière')
parser.add_argument('--csv', help='CSV file with informations (see pitie.csv)')
parser.add_argument('--run_name', help='Name of the run to use/create. It is not defined in CSV file')
parser.add_argument('--run_date', help='Date of launch ot the run')




def demoWriteRunOnServer(server, ssl, user, password):
    """
    This demo requires a server/login with write access.
    It creates patients/set/runs, upload data, and run analysis.
    Do not spam a production server!
    """

    # Login
    vidjil = Vidjil(server, ssl=ssl)
    vidjil.login(user, password)

    vidjil.setGroup() # Fill here value of your group. 
    # A list of your available groups can be found in the log of this script after vidjil.login() on recent server version. 
    # On older, you should send us a mail to get it.

    # Look if a run with same name already exist to use it if available; else create it.
    # Take care that this function will made a filter on your avaialble runs.
    # if run name is a subset of existing one, it will be found either.
    runs = vidjil.getSets(set_type=vidjil.RUN, filter_val=args.run_name)
    print( f"Looking for runs with name: {args.run_name}")
    if len(runs["query"]) > 1:
        Exception(f"More than one run with same name exists: {len(runs)}")
        print( runs )
        vidjil.infoSets("Runs", runs["query"], vidjil.RUN, verbose=True)
        raise Exception
    elif len(runs["query"]) == 1:
        run_data = runs["query"][0]
        print( f"Run already exists; use it. Run found: {run_data}")
        setid_run =  run_data['sample_set_id']
    elif len(runs["query"]) == 0:
        print( f"No previous run with same name; Creation if a new one: {args.run_name}")
        run_data = vidjil.createRun(args.run_name, run_date=args.run_date)
        setid_run = run_data["args"]["id"]
        
    print( "==> set run: %s" % setid_run)

    
    
    with open(args.csv) as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=';')
        for row in spamreader:
            print( row )

            # In all case, create a new patient with corresponding informations.
            # If this script is relaunch, they will be added again
            patient_data = vidjil.createPatient(first_name=row["first_name"], last_name=row["last_name"], birth_date=row["birth"], info=row["comments"])
            setid_patient = patient_data["args"]["id"]
            print( "==> new set patient: %s" % setid_patient)

            # Look if a generic set with same name already exist to use it if available; else create it.
            if row["generic_set"] != "":
                sets_other = vidjil.getSets(set_type=vidjil.SET, filter_val=row["generic_set"])
                if len(sets_other["query"]) > 1:
                    Exception(f"More than one generic sert with same name exists: {len(sets_other)}")
                    vidjil.infoSets("Sets", sets_other["query"], vidjil.SET, verbose=True)
                    raise Exception
                elif len(sets_other["query"]) == 1:
                    set_other = sets_other["query"][0]
                    print( f"Generic set already exists; use it. Generic set found: {set_other}")
                    set_other_id = set_other['sample_set_id']
                    # print(set_other)
                elif len(sets_other["query"]) == 0:
                    print( f"Create new generic set: {row["generic_set"]}")
                    set_data = vidjil.createSet(row["generic_set"], info="")
                    set_other_id = set_data["args"]["id"]

                print( "==> set other: %s" % set_other_id)

            # set_ids filed take value in a specific format: ':$set+($id)'
            # Multiple field should be separated with a '|' as above
            # With :
            #   $set can be 's' (generic set), 'p' (patient), or 'r' (run)
            #   $id is the id of the main set 
            setid_generic = f':s+({set_other_id})|' if row["generic_set"] != "" else "" # compose id string depending of generic set present or not
            sample = vidjil.createSample(source="computer",
                        pre_process= "4",
                        set_ids= f"{setid_generic}:r+({setid_run})|:p+({setid_patient})",
                        file_filename= row["file_r1"],
                        file_filename2= row["file_r2"],
                        file_id= "",
                        file_sampling_date= row["sampling_date"],
                        file_info= row["info"],
                        file_set_ids= "",
                        sample_set_id= setid_run,
                        sample_type= "run")

            file_id  = sample["file_ids"][0]  ## Uploaded file
            print( "==> new file %s" % file_id)

            
            ### Launch analysis on this new sample
            config_id = 2 ## IGH sur app; TRG = 30
            analysis  = vidjil.launchAnalysisOnSample(setid_run, file_id, config_id)
            print("Launch analysis: %s" % analysis)


if  __name__ =='__main__':
    """Examples using Vidjil API"""

    args = parser.parse_args()

    if PUBLIC_USER == "":
        PUBLIC_USER = input("Enter public login user:")
    if PUBLIC_PASSWORD == "":
        print( "Attempted to log as '%s' on public server" % PUBLIC_USER)
        PUBLIC_PASSWORD = getpass.getpass("Password for public server:")


    demoWriteRunOnServer(PUBLIC_SERVER, PUBLIC_SSL, PUBLIC_USER, PUBLIC_PASSWORD)