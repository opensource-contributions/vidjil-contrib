# Contributions to vidjil project

This repository is linked to [vidjil](https://gitlab.inria.fr/vidjil/vidjil) software.

Find more information about it at [https://www.vidjil.org/](https://www.vidjil.org/) software.

This repository allow to share vidjil community scripts used in vidjil instance or in your laboratories.

Here is a reminder of precess organisation of vidjil pipeline.

``` mermaid
graph LR
  A[sequence files] --> |preprocess| B[prepared sequence files];
  A --> C[vidjil files];
  B --> C;
  C -->|Fuse| D[fused file];
  C --> |prefuse|E[augmented vidjil];
  E --> D;
  D ---->|Post-fuse| F[Augmented fused file];
  D --> G[vidjil client];
  F --> G;
```



They are splitted in categories:

* **preprocess**: this is a set of scripts launched in sequences files before analysis by vidjil-algo (merge, trimming, ...)
* **prefuse**: this is a set of scripts launched on each file of analysis produces by vidjil-algo, before the fuse step
* **postfuse**: This is a set of scripts launched after the fuse processing of all samples analysis
* **external**: This is a set of script launch outside of vidjil to manipulate sequence or results files
* **third party softwares**: A set of software that can be used in other scripts
* **addons**: This is a set of script integrated to client side. Some functions to overload informations panel, show specific data added by some custom fuse script
* **API**: script that use api_vijdil.py to communicate with a vidjil server (crate patient, uplaod data, download result for meta-analysis)
