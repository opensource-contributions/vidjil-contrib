# Preprocess



``` mermaid
graph LR
  A[sequence files] --> |preprocess| B[prepared sequence files];
  A --> C[vidjil files];
  B --> C;
```


## Mandatory arguments

First arguement are deliver by server and will be given in this exact order to script.

* **binary_dir**: path to executable
* **file_R1**: forward read file
* **file_R2**: reverse read file
* **output_file**: output file

Script need to implement an arguments parsing with these values at least.
See exemple in samples directory.