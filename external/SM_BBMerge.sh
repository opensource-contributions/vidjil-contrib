#!/bin/bash

## SM_BBMerge v1.5 09/02/2021
## Usage: SM_BBMerge.sh InputDir 
## les outputs se trouveront dans Inputdir/MERGE/PCRMerge
## par sample:
## >1 fichier contig,
## >1 fichier FR2vdjl (reads overlapant mergés),
## >1 fichier Avdjl (reads mergés et étendus totaux),
## >1 fichier Hvdjl (reads mergés et étendus les plus longs,>320nt)
## Author; Kaddour Chabane kaddour.chabane@chu-lyon.fr

if [ $# -ne 1 ]; then
	echo "Usage: SM_BBMerge.sh InputDir" 1>&2
	exit 1
fi

# tell bash to be verbose and to abort on error
set -o pipefail
set -x -e -u

# get arguments

inputdir="$1"

# Concatenate fastq

cd $inputdir; \
/srv/nfs/ngs-stockage/NGS_Hematologie_GHS/pipeLine_Lyon/merge.sh;cd; 




# primers trimming 5' R1 & R2 (25 first bases)

mkdir -p $inputdir/MERGE/PCRMerge; \

for fq1 in $(ls $inputdir/MERGE/*_R1.fastq.gz); do
fq2=${fq1/%_R1.fastq.gz/_R2.fastq.gz}; \
if [ -f $fq2 ]; then smpl=$(basename $fq1 _R1.fastq.gz); \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/bbduk.sh \
in=$fq1 in2=$fq2 \
out=$inputdir/MERGE/PCRMerge/$smpl-BBduk_R1.fastq.gz \
out2=$inputdir/MERGE/PCRMerge/$smpl-BBduk_R2.fastq.gz \
forcetrimleft=24 qtrim=rl trimq=10 minlen=30

fi; done \

# # primers trimming 5' R1 & R2 (according to their sequences)

# mkdir -p $inputdir/MERGE/PCRMerge; \

# for fq1 in $(ls $inputdir/MERGE/*_R1.fastq.gz); do
# fq2=${fq1/%_R1.fastq.gz/_R2.fastq.gz}; \
# if [ -f $fq2 ]; then smpl=$(basename $fq1 _R1.fastq.gz); \

# /srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/bbduk.sh \
# in=$fq1 in2=$fq2 \
# out=$inputdir/MERGE/PCRMerge/$smpl-BBduk_R1.fastq.gz \
# out2=$inputdir/MERGE/PCRMerge/$smpl-BBduk_R2.fastq.gz \
# ref=/srv/nfs/ngs-stockage/NGS_Hematologie_GHS/pipeLine_Lyon/scripts/Script_SM/doc/primers_IGHV.fasta \
# hdist=1 ktrim=l rcomp=t k=20 mink=8 qtrim=rl trimq=10 minlen=30 overwrite=true restrictleft=25 ordered=t skipr2=f

# fi; done \

# contigs generation

for fq1 in $(ls $inputdir/MERGE/PCRMerge/*BBduk_R1.fastq.gz); do
fq2=${fq1/%_R1.fastq.gz/_R2.fastq.gz}; \
if [ -f $fq2 ]; then smpl=$(basename $fq1 -BBduk_R1.fastq.gz); \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/tadpole.sh \
in=$fq1 in2=$fq2 \
out=$inputdir/MERGE/PCRMerge/$smpl-contig.fastq.gz \
k=44 contigpasses=36 rcomp=t mincountseed=8 mincountextend=5 mincontig=100 mincoverage=10

fi; done \

# overlaping R1-R2 merging (only FR2 amplicons for clone proportion in vidjil)
for fq1 in $(ls $inputdir/MERGE/*_R1.fastq.gz); do
fq2=${fq1/%_R1.fastq.gz/_R2.fastq.gz}; \
if [ -f $fq2 ]; then smpl=$(basename $fq1 _R1.fastq.gz); \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/bbmerge.sh \
in=$fq1 in2=$fq2 \
out=$inputdir/MERGE/PCRMerge/$smpl-FR2vdjl.fastq.gz

fi; done \

# PCR R1&R2 merging (overlaping and no overlaping)

for fq1 in $(ls $inputdir/MERGE/PCRMerge/*BBduk_R1.fastq.gz); do
fq2=${fq1/%_R1.fastq.gz/_R2.fastq.gz}; \
if [ -f $fq2 ]; then smpl=$(basename $fq1 -BBduk_R1.fastq.gz); \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/bbmerge-auto.sh \
in=$inputdir/MERGE/PCRMerge/$smpl-BBduk_R1.fastq.gz \
in2=$inputdir/MERGE/PCRMerge/$smpl-BBduk_R2.fastq.gz \
out=$inputdir/MERGE/PCRMerge/$smpl-mergedPCR.fastq.gz \
ecct extend2=50 iterations=7

fi; done \

# small merged reads removing

for input in $(ls $inputdir/MERGE/PCRMerge/*-mergedPCR.fastq.gz); do
Houtput=${input/%-mergedPCR.fastq.gz/-HmergedPCR.fastq.gz} \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/bbduk.sh \
in=$input \
out=$Houtput \
minlen=180 ordered=t

done \

# 5' read extension

for input in $(ls $inputdir/MERGE/PCRMerge/*-HmergedPCR.fastq.gz); do
LxtdOutput=${input/%-HmergedPCR.fastq.gz/-LxtdPCR.fastq.gz} \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/tadpole.sh \
in=$input \
out=$LxtdOutput \
mode=extend extendleft=320 ibb=t rcomp=t k=60

done \

# 3' read extension

for input in $(ls $inputdir/MERGE/PCRMerge/*-LxtdPCR.fastq.gz); do
LRxtdOutput=${input/%-LxtdPCR.fastq.gz/-Avdjl.fastq.gz} \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/tadpole.sh \
in=$input \
out=$LRxtdOutput \
mode=extend extendright=320 ibb=t rcomp=t k=60

done \

# Longest reads selection

for input in $(ls $inputdir/MERGE/PCRMerge/*-Avdjl.fastq.gz); do
Foutput=${input/%-Avdjl.fastq.gz/-Hvdjl.fastq.gz} \

/srv/nfs/ngs-stockage/NGS_commun/disnap/bioInfo/scripts/BBMap_36.32/bbmap/bbduk.sh \
in=$input \
out=$Foutput \
minlen=320 ordered=t

done \



# Temporary files removing

rm $inputdir/MERGE/PCRMerge/*BBduk*; \
rm $inputdir/MERGE/PCRMerge/*PCR.fastq.gz; \
rm $inputdir/*.fastq.gz; \

exit 0