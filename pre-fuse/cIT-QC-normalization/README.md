Program to perform cIT-QC normalization in .vidjil files.

Developed at the Boldrini Research Center, Brazil, in 2023.

Contributors: 
  Antonio Vítor Ribeiro
  Guilherme Navarro Nilo Giusti
  João Meidanis

This script allow to make a normalization based on MRD configuraton given and spikin reads found in a sequencing file.

To work, this script should be sibling of a configuration file that will be call by vidjil algo to label specific clonotype, and by fuse preprocess to make normalisation. 
A prefill configuration file is given with EuroMRD data (file `cIT-QC.json`)

An example of configuration that use this script:

* Vidjil command: `-c clones -z 100 -r 1 -g germline/homo-sapiens.g -e 1 -2 -d -w 50 --label-json  /usr/share/vidjil/tools/$path/cIT-QC.json`
* Fuse command: `-t 100 -d lenSeqAverage -t 100  --pre "cIT-QC-normalization.py --config cIT-QC-mod.json  --verbose"`

Note that for vidjil, a complete path should be given, including `$path` that is the path given for processus in `def.py script` of `/usr/share/vidjil/tools` directory (by default "./"), but just the name of the file for fuse command.