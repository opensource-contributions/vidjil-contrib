#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Program to perform cIT-QC normalization in .vidjil files.

    Developed at the Boldrini Research Center, Brazil, in 2023.
    
    Contributors: 
      Antonio Vítor Ribeiro
      Guilherme Navarro Nilo Giusti
      João Meidanis

'''

############################################################
### imports

from __future__ import print_function
from collections import defaultdict
import json
import sys
import os
import argparse

############################################################
### constants


############################################################
### routines

# gets prevalent germline from sample
def getPrevalent(germlines):
    ''' 
    The prevalent germline is the maximum found germline
    If prevalent is IGK+ and IGK > 10% of IGK+, return IGK
    '''
    maxi      = max(germlines, key=lambda key: germlines[key][0])
    prevalent = "" if germlines[maxi][0] == 0 else maxi
    # Specific case of IGK+
    if prevalent == "IGK+" and "IGK" in reads.keys() and reads["IGK"] != 0:
        if float(reads["IGK"])/float(reads["IGK+"]) > 0.1:
            prevalent = "IGK"
    return  prevalent

# returns dictionary with label clones
def getControls(data, prevalent):
    label_clones  = [spike for spike in data if 'label' in spike.keys()]
    spikes_clones = filter(lambda spike: len([germ for germ in germ_dict[prevalent] if germ in  spike['label']]), label_clones)
    return spikes_clones

# calculates total control cells and reads
def getTotalReadsAndCells(controls, copy_num):
    total_spikes = [spike["reads"][0] for spike in controls]
    unique_spike = [spike["label"] for spike in controls]

    total_c_reads = sum(total_spikes)
    total_c_cells = len(set(unique_spike)) * copy_num
    return total_c_reads, total_c_cells

# calculates MRD value
def calcMRD(data, total_c_reads, total_c_cells, total_cells, ssr):
    factor = float(total_c_cells) / float(total_c_reads)
    factor_cells = factor / float(total_cells)
    for spike in data['clones']:
        spike['normalized_reads'] = [spike['reads'][0] * float(factor_cells) * ssr]
    return data, factor

# checks for missing cIT-QC
def verification(controls, prevalent):
    con     = [spike["label"] for spike in controls]
    missing = [spike for spike in spike_dict[prevalent] if spike not in con]
    
    if len(missing) > 0:
        return "cIT-QC not found: %sx\n\t%s" % (len(missing), "\n\t".join(missing))
    else:
        return "All cIT-QC have been found" 


def extractSpikeDict(data):
    spike_dict = defaultdict(lambda: [])
    for spike in data["config"]["labels"]:
        spike_dict[spike["locus"]].append(spike["name"])
    return spike_dict

############################################################
### main program

if __name__ == '__main__':

    print("#", ' '.join(sys.argv))


    DESCRIPTION = 'Script to include cIT-QC normalization on a vidjil result file'
    
    parser = argparse.ArgumentParser(description= DESCRIPTION,
                                     epilog='''Example: python %(prog)s -- input filein.vidjil --output fileout.vidjil --config conf_mrd.json''',
                                     formatter_class=argparse.RawTextHelpFormatter)

    group_options = parser.add_argument_group()
    group_options.add_argument('-i', '--input', help='Vidjil input file')
    group_options.add_argument('-o', '--output', help='Vidjil output file with cIT-QC normalization')
    group_options.add_argument('-c', '--config', help='cIt-QC configuration')
    group_options.add_argument('--verbose', action='store_true', help='run script in verbose mode')
    
    args = parser.parse_args()

    inf  = args.input
    outf = args.output

    # Both script and config file should be in the same directory
    # For security reason, complete path can't be given in process configuration. 
    # so we use current path of this script as path for configuration file
    conf = args.config if os.path.isfile(args.config) else os.path.dirname(__file__)+os.path.sep+args.config
    msgs = args.verbose
    print ( "verbose: %s" % msgs)
    
    ## Load configuration
    with open(conf) as conf_file:
        config      = json.load(conf_file)
        conf_name   = config["config"]["name"]
        germ_dict   = config["config"]["germ_dict"]  # Vidjil germline > EuroClonality junction classes
        spike_dict  = extractSpikeDict(config)       # Vidjil germline > EuroClonality cIT-QC
        copy_num    = config["config"]["copy_num"]
        total_cells = config["config"]["total_cells"]
        log  = "MRD pipeline post-processus\nLoaded conf: %s\n" % conf_name


    ## read input file
    with open(inf) as inp:
        data = json.load(inp)

    ## process data
    ssr = data['reads']['segmented'][0]
    prevalent = getPrevalent(data['reads']['germline'])
    log += "Prevalent germline: %s\n" % prevalent
    if prevalent == False:
        log += "Warnings: No clonotypes found in this vidjil file. No MRD computable\n"
    else:
        controls = getControls(data['clones'], prevalent)
        total_c_reads, total_c_cells = getTotalReadsAndCells(controls, copy_num)
        updated_data, factor = calcMRD(data, total_c_reads, total_c_cells, total_cells, ssr)
        warnings = verification(controls, prevalent)

        log += "Reads: %s/%s reads\n" % (total_c_reads, data['reads']['germline'][prevalent][0])
        log += "Quantification factor: %s\n" % factor
        log += "Warnings: %s" % warnings
    
    if msgs:
        print(log)
    
    ## Write log in file
    if not "outter_process" in updated_data["samples"]:
        updated_data["samples"]["outter_process"] = {}
    updated_data["samples"]["outter_process"]["cIT-QC normalization"] = {"log": log}

    ## write output file
    with open(outf, 'w') as of:
        print(json.dumps(updated_data, sort_keys=True, indent=2), file=of)
