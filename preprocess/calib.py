"""
This script use some third party software. 


"""


from os import listdir, path, makedirs, path
from os.path import isfile, join
import sys
import shutil
import gzip
import subprocess
import argparse
import shlex
import os
from logparser import *
import tempfile
import json
from datetime import datetime
import pandas as pd
import io


parser = argparse.ArgumentParser(description='Use CALIB umi demultiplexer to make a new fastq file')

parser.add_argument("binaries_dir", help="path to binaries executable")
parser.add_argument("file_R1", help="forward read file")
parser.add_argument("file_R2", help="reverse read file")
parser.add_argument("output_file", help="output file")
parser.add_argument("-r1", "--keep_r1", help="keep unmerged forward reads", action="store_true")
parser.add_argument("-r2", "--keep_r2", help="keep unmerged reverse reads", action="store_true")
parser.add_argument("-k", "--keep", help="keep temporary files (may take lots of disk space in the end)", action = 'store_true')
parser.add_argument("--calib-options",      help="additional options passed for calib binaries", default="")
parser.add_argument("--calib_cons-options", help="additional options passed for calib cons",     default="")
parser.add_argument("--flash2-options",     help="additional options passed for flash2",         default="")

args  = parser.parse_args()
f_r1  = args.file_R1
f_r2  = args.file_R2
f_out = args.output_file
opt_calib      = args.calib_options
opt_calib_cons = args.calib_cons_options
opt_flash2     = args.flash2_options


paths =  os.path.split(f_out)
path_head = paths[0]
path_file = paths[1]



##################
### Some functions
##################
def remove_file(filename):
    print( "*** remove file: "+filename )
    try:
        os.remove(filename)
    except:
        print("\t file not found")
    return

def transform_stats(pstats_stdoutdata):
    """ Convert seqkit data into pandas dataframe to export it as formated data inside log """
    csv = io.StringIO(pstats_stdoutdata.decode())
    df  = pd.read_csv(csv, delim_whitespace=True).transpose() # swap to get all col values at proximity in log

    log     = ""
    files   = ["R1_raw", "R2_raw", "R1_demultiplexed", "R2_demultiplexed", "merged_file"]
    df.columns = files
    print(df.to_string())
    
    lines = ["file", "format", "type", "num_seqs", "sum_len", "min_len", "avg_len", "max_len"]
    lines_to_keep = ["num_seqs", "min_len", "avg_len", "max_len"]
    for line in lines_to_keep:
        for filetype in files:
            # print( "%s %s: %s" % (filetype, col, df.at[files.index(filetype), col]))
            # log += f"{filetype} {col}: {df.at[files.index(filetype), col]}\n"
            print( f"{line}: {lines.index(line)}, {filetype}: {files.index(filetype)}")
            log += f"[STATS] {line} {filetype}: {df.at[line, filetype]}\n"
    # warnings
    if df.at["num_seqs", "R1_raw"] != df.at["num_seqs", "R2_raw"]:
        log += "[STATS] warning: Raw sequence files haven't same number of reads (%s vs %s)\n" % (df.at[0, "num_seqs"], df.at[1, "num_seqs"])
    if df.at["num_seqs", "R1_demultiplexed"] != df.at["num_seqs", "R2_demultiplexed"]:
        log += "[STATS] warning: demultiplexed sequence files haven't same number of reads (%s vs %s)\n" % (df.at[2, "num_seqs"], df.at[3, "num_seqs"])
    if df.at["num_seqs", "R1_raw"] == df.at["num_seqs", "R2_raw"] and df.at["num_seqs", "R1_demultiplexed"] == df.at["num_seqs", "R2_demultiplexed"]:
        reads_init = float(df.at["num_seqs", "R1_raw"].replace(",", ""))
        reads_end  = float(df.at["num_seqs", "R1_demultiplexed"].replace(",", ""))
        log += "[STATS] Division ratio of demultiplexion: %s\n" % round((reads_init / reads_end), 5)
        log += "[STATS] Percentage keep after demultiplexion: %s\n" % (float(100 * reads_end) / reads_init)
    return log, df

def exist_color(exist):
    color = "\033[32m" if exist == True else "\033[91m"
    return "%s%s\033[0m" % (color, exist)


def filter_log_content(content):
    # Filter some defined line of log that are not important in client side
    to_filter = [
        "[CALIB] Thread 0", "[CALIB] On thread", "[CALIB] Adding edges", "[CALIB] Extracting", "[CALIB] Outputting",
        "[CALIB_CONS] b'Reading", "[CALIB_CONS] Writing",
        "[FLASH] Input", "[FLASH] Output", "[FLASH]     Input", "[FLASH]     Output"
    ]
    for searched in to_filter:
        if searched in content:
            return False
    return True

#######################
### Recap of parameters
#######################
print( "args: %s" % args)
print( "###" )
print( "f_r1: %s" % f_r1)
print( "f_r2: %s" % f_r2)
print( "f_out: %s" % f_out)
print( "path_head: %s" % path_head)
print( "path_file: %s" % path_file)
print( "calib options: '%s'" % opt_calib)
print( "calib cons options: '%s'" % opt_calib_cons)
print( "flash2 options: '%s'" % opt_flash2)
print( "keep temporary files: '%s'" % args.keep)
print( "keep R1: '%s'" % args.keep_r1)
print( "keep R2: '%s'" % args.keep_r2)
f_phase1 = path_head+"/"+path_file+"."


print( "\n===== Availability of software")
exit_calib      = path.exists('%s/calib' % args.binaries_dir)
exit_calib_cons = path.exists('%s/calib_cons' % args.binaries_dir)
exit_flash2     = path.exists('%s/flash2' % args.binaries_dir)
exit_seqkit     = path.exists('%s/seqkit' % args.binaries_dir)
print ("calib exists: "      + exist_color(exit_calib))
print ("calib cons exists: " + exist_color(exit_calib_cons))
print ("flash2 exists: " + exist_color(exit_flash2))
print ("seqkit exists: " + exist_color(exit_seqkit))
print( "=======================\n")



#################################
### Build a calib parameters file
#################################
cmd = ['%s/calib' % args.binaries_dir, "-f", f_r1, "-r", f_r2, "-o", f_phase1]
cmd += shlex.split( "-e 0 -l 3 -m 7 -t 7 -k 8" )
if f_r1.endswith(".gz") and f_r2.endswith(".gz"):
    cmd += shlex.split( "-g" )
    os.system("gunzip -f -k %s %s" % (f_r1, f_r2) )
cmd += shlex.split( opt_calib )

print( "Phase 1/3 (calib clusterisation index)")
print( "\t# %s" % cmd )
process_1 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
(p1_stdoutdata, p1_stderrdata) = process_1.communicate()

if process_1.returncode > 0:
    raise EnvironmentError("\tPhase 1/3 FAILED")
else:
    print( "\tPhase 1/3 finished")


###################################
### Rebuild file from UMI cluster
###################################


### Build a calib parameters file
cmd = ['%s/calib_cons' % args.binaries_dir,
 "-c", f_phase1+"cluster",
 "-q", f_r1.replace(".gz",""), f_r2.replace(".gz",""),
 "-o", path_head+"/R1.", path_head+"/R2."
]
cmd += shlex.split( opt_calib_cons )

print( "Phase 2/3 (calib clusterisation of fastq files)")
print( "\t# %s" % cmd )
process_2 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
(p2_stdoutdata, p2_stderrdata) = process_2.communicate()
if process_2.returncode > 0:
    raise EnvironmentError("\tPhase 2/3 FAILED")
else:
    print( "\tPhase 2/3 finished")


###################################
### Merge result files with flash2
cmd = ['%s/flash2' % args.binaries_dir,
 path_head+"/R1..fastq", path_head+"/R2..fastq",
 "-d", path_head,
 "-o", path_file,
 "-t", "1",
]
cmd += shlex.split( opt_flash2 )

print( "Phase 3/3 (flash2 merge of filtrered files)")
print( "\t# %s" % cmd )
process_3 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
(p3_stdoutdata, p3_stderrdata) = process_3.communicate()
if process_3.returncode > 0:
    raise EnvironmentError("\tPhase 3/3 FAILED")
else:
    print( "\tPhase 3/3 finished")


###################################
### Rebuild a merged file as usual
try :
    with gzip.open(f_out, 'w') as outFile:
        with open(f_out+'.extendedFrags.fastq', 'rb') as f1:
            shutil.copyfileobj(f1, outFile)
        if (args.keep_r1):
            with open(f_out+'.notCombined_1.fastq', 'rb') as f2:
                shutil.copyfileobj(f2, outFile)
        if (args.keep_r2):
            with open(f_out+'.notCombined_2.fastq', 'rb') as f3:
                shutil.copyfileobj(f3, outFile)

    ##############################################
    ### Stats on preprocess; count number of reads
    # Use seqkit toolbox (https://github.com/shenwei356/seqkit)
    cmd_stats = '%s/seqkit stats %s %s %s %s %s' % (args.binaries_dir, f_r1.replace(".gz",""), f_r2.replace(".gz",""), path_head+"/R1..fastq", path_head+"/R2..fastq", path_head+"/"+path_file)
    print( "\t# %s" % cmd_stats )
    process_stats = subprocess.Popen(shlex.split(cmd_stats), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (pstats_stdoutdata, pstats_stderrdata) = process_stats.communicate()
    if process_stats.returncode > 0:
        raise EnvironmentError("\tPhase stats FAILED")
    else:
        print( "\tPhase stats finished")


    log_stats, stats = transform_stats(pstats_stdoutdata)



    ## Create a json log file
    output_file = '%s/pre_process.vidjil' % path_head
    with tempfile.NamedTemporaryFile(delete=False, mode="w+", encoding="utf-8") as logfile:
        logfile_name = logfile.name
        print( f"\n============\nLOGFILE WRITE STDOUTDATA: {logfile.name}")
        logfile_content = []
        str(p1_stdoutdata).split("\n")
        logfile_content += ["[CALIB] "+elt+"\n"      for elt in str(p1_stdoutdata).split("\\n")]
        logfile_content += ["[CALIB_CONS] "+elt+"\n" for elt in str(p2_stdoutdata).split("\\n")]
        logfile_content += [ elt+"\n"  for elt in str(p3_stdoutdata).split("\\n")]
        logfile_content += [ elt+"\n"  for elt in str(log_stats).split("\n")]

        # Filter content of noise log line
        filtered_logs = filter(filter_log_content, logfile_content)

        for content in filtered_logs:
            if ":" in str(content):
                logfile.write( str(content) )
        logfile.seek(0)
    
    logfile.close()
    logfile_copy = path_head+"/calib_preprocess.log"
    shutil.copyfile(logfile_name, logfile_copy)

    log_parser = LogParser(logfile_copy)
    log_parser.export("CALIB; args from preprocess", output_file)

    with open(output_file, 'r') as vidjil_file:
        parsed_log = json.load(vidjil_file)

        if not "stats" in parsed_log["pre_process"].keys():
            parsed_log["pre_process"]["stats"] = {}

        keys = [key for key in parsed_log["pre_process"].keys()]
        for key in keys:
            if key not in ["commandline", "run_timestamp", "stats"]:
                parsed_log["pre_process"]["stats"][key] = parsed_log["pre_process"][key]
                del parsed_log["pre_process"][key]

        parsed_log["reads"]["merged"] = [int(stats.at["num_seqs", "merged_file"].replace(",", ""))]
        parsed_log["reads"]["total"]  = [int(stats.at["num_seqs", "R1_demultiplexed"].replace(",", ""))]
        parsed_log["reads"]["raw"]    = [int(stats.at["num_seqs", "R1_raw"].replace(",", ""))]


    with open(output_file, 'w') as vidjil_file:
        vidjil_file.write(json.dumps(parsed_log))

    print( "\n=========\nparsed_log\n" )
    print( parsed_log )

except IOError :
    print("Failed to rebuild merged file")
    raise

finally:
    ## Remove temporary files
    print( "\n==========\nRemove temporary files:")
    if ".gz" in f_r1: # unzipped fastq file if not raw data
        remove_file(f_r1.replace(".gz",""))
        remove_file(f_r2.replace(".gz",""))
    if not args.keep:
        files_to_remove  = [f_out+'.cluster', path_head+"/R1..fastq", path_head+"/R2..fastq", path_head+"/R1..msa", path_head+"/R2..msa", f_out+'.extendedFrags.fastq', f_out+'.notCombined_1.fastq', f_out+'.notCombined_2.fastq']
        files_to_remove += [f_out+'.hist', f_out+'.histogram'] ## Remove the histogram provide by Flash2
        for file_to_remove in files_to_remove:
            remove_file(file_to_remove)
    else: 
        print("Keep temporary files")
        raise()


