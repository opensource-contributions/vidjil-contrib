from os import listdir, path, makedirs
from os.path import isfile, join
import sys
import shutil
import gzip
import subprocess
import argparse
import shlex
import os
# from logparser import FlashLogParser
import tempfile
import json
from datetime import datetime


# ==========================
# This script make a merging step, followed by vdj filtering made by vidjil-algo
# This allow to store only a shrinked file with only relevant informations for vdj identification
# This allow to speed up following analysis and reduce disk space taken by sequences
# ==========================

# ===========================================
### Import script from tools parent directory
# Add parent path to sys
import sys
import os
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(os.path.dirname(current))
sys.path.append(parent)
# ==========================
from logparser import *
# ==========================

parser = argparse.ArgumentParser(description='Use FLASH2 read merger to make a new fastq file and keep unmerged reads')

parser.add_argument("flash2_dir", help="path to flash2 executable")
parser.add_argument("file_R1", help="forward read file")
parser.add_argument("file_R2", help="reverse read file")
parser.add_argument("output_file", help="output file")
parser.add_argument("-r1", "--keep_r1", help="keep unmerged forward reads", action="store_true")
parser.add_argument("-r2", "--keep_r2", help="keep unmerged reverse reads", action="store_true")
parser.add_argument("-f", "--flash2-options", help="additional options passed to FLASH2", default="")
parser.add_argument("-k", "--keep", help="keep temporary files (may take lots of disk space in the end)", action = 'store_true')


args  = parser.parse_args()
f_r1  = args.file_R1
f_r2  = args.file_R2
f_out = args.output_file
f_opt = args.flash2_options

file_merge     = tempfile.NamedTemporaryFile(delete=False)
file_merge_out = tempfile.NamedTemporaryFile(delete=False)
file_vidjil    = tempfile.NamedTemporaryFile(delete=False)

paths =  os.path.split(f_out)
path_head = paths[0]
path_file = paths[1]
print( "args: %s" % args)
print( "###" )
print( "f_r1: %s" % f_r1)
print( "f_r2: %s" % f_r2)
print( "f_out: %s" % f_out)
print( "flash2 options: %s" % f_opt)
print( "path_head: %s" % path_head)
print( "path_file: %s" % path_file)
print( "file_merge: %s" % file_merge.name)
print( "file_merge_out: %s" % file_merge_out.name)



## Phase 1, read smerger with Flash2
cmd_flash = ['%s/flash2' % args.flash2_dir,
 f_r1, f_r2,
 "-d", "/",
 "-o", file_merge.name, # file prefix
 "-t", "1",
 # "--compress"
]
cmd_flash += shlex.split( f_opt )
print( "# %s" % cmd_flash )

p_merger = subprocess.Popen(cmd_flash, stdout=subprocess.PIPE, text=True)

(stdoutdata_flash, stderrdata_flash) = p_merger.communicate()

if p_merger.returncode > 0:
    print( stdoutdata_flash)
    print( stderrdata_flash)
    raise EnvironmentError("Flash2 failed")


try :
    with gzip.open(file_merge_out.name, 'w') as outFile:
        print( "# 1")
        with open(file_merge.name+'.extendedFrags.fastq', 'rb') as f1:
            shutil.copyfileobj(f1, outFile)
        print( "# 2")
        if (args.keep_r1):
            with open(file_merge.name+'.notCombined_1.fastq', 'rb') as f2:
                shutil.copyfileobj(f2, outFile)
        print( "# 3")
        if (args.keep_r2):
            with open(file_merge.name+'.notCombined_2.fastq', 'rb') as f3:
                shutil.copyfileobj(f3, outFile)
        print( "# 4")
        if not args.keep:
            os.remove(file_merge.name+'.extendedFrags.fastq')
            os.remove(file_merge.name+'.notCombined_1.fastq')
            os.remove(file_merge.name+'.notCombined_2.fastq')
            os.remove(file_merge.name)

        print( "# 5")
        ## Remove the histogram provide by Flash2
        os.remove(file_merge.name+'.hist')
        os.remove(file_merge.name+'.histogram')
except:
    print( stdoutdata_flash)
    print( stderrdata_flash)
    raise EnvironmentError("don't work")

## Phase 2; vidjil-algo filter
germlinefile = f"{current}/flash2_and_vdj_filter.g" 
cmd_filter = ['/usr/share/vidjil/vidjil-algo',
 "-g", germlinefile,
 "--filter-reads",
 "--dir", "/",
 "--base", file_vidjil.name,
 file_merge_out.name
]
print( "# %s" % cmd_filter )

p_filter = subprocess.Popen(cmd_filter, stdout=subprocess.PIPE, text=True)

(stdoutdata_filter, stderrdata_filter) = p_filter.communicate()

if p_filter.returncode > 0:
    print( stdoutdata_filter)
    print( stderrdata_filter)
    raise EnvironmentError("vidjil-algo filter failed")


## Phase 3; replace and compress filtered reads
cmd_move = ['mv', f"{file_vidjil.name}.detected.vdj.fa", f_out]
print( "# %s" % cmd_move )

p_move = subprocess.Popen(cmd_move, stdout=subprocess.PIPE, text=True)

(stdoutdata_move, stderrdata_move) = p_move.communicate()

if p_move.returncode > 0:
    print(stdoutdata_move)
    print(stderrdata_move)
    raise EnvironmentError("move filtered as output failed")





try :
    with tempfile.NamedTemporaryFile(mode="w+") as logfile:
        logfile.write(stdoutdata_flash)
        logfile.write(stdoutdata_filter)
        logfile.seek(0)

        output_file = '%s/pre_process.vidjil' % path_head
        log_parser  = FlashLogParser(logfile)
        log_parser.export(sys.argv, output_file)

    print("### Phase 1 : reads merger ###")
    print(stdoutdata_flash)
    print("\n\n\n### Phase 2 : vidjil algo filter ###")
    print(stdoutdata_filter)

except IOError :
    os.remove(f_out)
    raise
