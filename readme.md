# Contributions to vidjil project

This repository is linked to [vidjil](https://gitlab.inria.fr/vidjil/vidjil) software.

Find more information about it at [https://www.vidjil.org/](https://www.vidjil.org/) software.

This repository allow to share vidjil community script used in vidjil instance or in your laboratories.

They are splitted in 4 categories:
* preprocess: this is a set of scripts launched in sequences files before analysis by vidjil-algo (merge, trimming, ...)
* prefuse: this is a set of scripts launched on each file of analysis produces by vidjil-algo, before the fuse step
* postfuse: This is a set of scripts launched after the fuse processing of all samples analysis
* external: This is a set of script launch outside of vidjil to manipulate sequence or results files

All contributions are welcome.
Please send a mail at support@vidjil.org


## List of available scripts: 

### Preprocess:

* calib: Make a deduplication step of UMI sequencing file before launching vidjil analysis
* FilterByLength: Allow to make a filter of reads based on reads length. This can be usefull for primer dimers cleaning

### Pre-fuse:

* capture_contigs.py: Allow to make a De Novo asssembly for capture data and to get complete V sequence for LLC hypermutation analysis
* cIT-QC-normalization: A script that allow to make a normalisation of reads/clonotype based on a given set of spikin (built in with euroMRD sequences)

### Post-fuse:


## Third-party softwares

Some third-party software may be needed to execute scripts.
A list of them is present in `LICENCE.md` file, with author, version and sources links.

These softwares need to be compiled into the vidjil-server docker image to avoid 
compilation incompatibilities.
To automatically do this, the `Makefile` include rules that launch a docker image of vidjil-server and built third party softwares.

Resulting softwares will be available in `third-party-softwares/compiled` and should be moved/declared inside your `docker-compose.yml` of vidjil project.